# query using string search, accepts a client and an index to execute the query on
# uses the must_contain, must_not_contain and should_contain arrays to construct a boolean query
function es_search_query_strings(c::Client, index::String, query_terms::Array{String,1}; batch_mode = true, retrieve_source=false)
	params = construct_bool_query(create_string_query_params(query_terms))
	process_search_request(c, index, params, batch_mode, retrieve_source)
end

# query using a date range, constructs a date range query on the provided index on the client
# start and end date must be in MM-DD-YYYY format
function es_search_query_date(c::Client, index::String, date_field_name::String, start_date::String, end_date::String; batch_mode = true, retrieve_source=false)
	params = construct_bool_query(create_date_range_query_params(start_date, end_date, date_field_name))
	process_search_request(c, index, params, batch_mode, retrieve_source)
end

function es_search_query_string_and_date(c::Client, index::String, date_field_name, start_date, end_date, must_contain, must_not_contain, should_contain; batch_mode = true, retrieve_source = false)
	filter_params = create_string_query_params(must_contain)
	append!(filter_params, create_date_range_query_params(start_date, end_date, date_field_name))

	params = construct_bool_query(filter_params)
	process_search_request(c, index, params, batch_mode, retrieve_source)
end

function es_search_term_string(c::Client, index::String, field_name::String, terms::Array{String, 1}; batch_mode = true, retrieve_source = false)
	params = construct_bool_query(create_string_term_params(field_name, terms))
	process_search_request(c, index, params, batch_mode, retrieve_source)
end

function es_search_term_string_and_date(c::Client, index::String, date_field_name::String, start_date::String, end_date::String, field_name::String, terms::Array{String, 1}; batch_mode = true, retrieve_source = false)
	filter_params = create_string_term_params(field_name, terms)
	append!(filter_params, create_date_range_query_params(start_date, end_date, date_field_name))
	params = construct_bool_query(filter_params)
	process_search_request(c, index, params, batch_mode, retrieve_source)
end

function es_get_all_documents(c::Client, index::String; batch_mode = true, retrieve_source = false)
	process_search_request(c, index, construct_bool_query(create_match_all_query_params()), batch_mode, retrieve_source)
end

# constucts a boolean query parameter object based on the must, must_not and should parameter lists
function construct_bool_query(filter_params::Paramlist)
	bool_dict = Dict{String, Any}()
	bool_dict["filter"] = filter_params

	params = Dict{String, Any}()
	params["query"] = Dict{String, Any}()
	params["query"]["bool"] = bool_dict
	params["from"] = 0
	params["size"] = 1000
	params
end

# processes a constructed query on an index on the client
# in batch mode, it collects all found documents and returns them
# in streaming mode, the total number of hits, the first 1000 documents and the current scroll id are returned which can be used with get_next_scroll
function process_search_request(c::Client, index::String, json_search_params::Dict{String, Any}, batch_mode, retrieve_source)
  if !retrieve_source json_search_params["_source"] = false end
	resp = post(string(c.connection_string, index, "/_search?scroll=10m"), data=JSON.json(json_search_params))
	total_hits = 0
	scroll_id = -1
	resp = json(resp)
	if haskey(resp, "hits") && resp["hits"]["total"] > 0
		total_hits = resp["hits"]["total"]
		scroll_id = resp["_scroll_id"]
		all_hits = resp["hits"]["hits"]
	else
		return []
	end

  if !batch_mode
    return (total_hits, all_hits, scroll_id)
  end

	hits_processed = length(all_hits)
  while hits_processed < total_hits
    (hits, scroll_id) = get_next_scroll(c, scroll_id)
    hits_processed += length(hits)
    append!(all_hits, hits)
  end
	all_hits
end

function get_next_scroll(c::Client, scroll_id::String)
  scroll_params = Dict("scroll" => "1m", "scroll_id" => scroll_id)
  resp = post(string(c.connection_string, "_search/scroll"), data=JSON.json(scroll_params))
	resp = json(resp)
  (resp["hits"]["hits"], resp["_scroll_id"])
end

function create_match_all_query_params()
	push!(Paramlist(), Dict{String, Any}("match_all" => Dict()))
end

# create a query string parameter based on an array of strings
function create_string_query_params(query_strings::Array{String,1})
	qs_params = Paramlist()
	for qs_string in query_strings
		field_dict = Dict{String,String}()
		field_dict["default_field"] = "_all"
		field_dict["query"] = qs_string
		push!(qs_params,Dict("query_string" => field_dict))
	end
	qs_params
end

function create_string_term_params(field_name::String, terms::Array{String,1})
	term_params = Paramlist()
	for term in terms
		field = Dict{String, Any}(field_name => term)
		push!(term_params, Dict{String, Any}("term" => field))
	end
	term_params
end

# create a date range query parameter
function create_date_range_query_params(start_date::String, end_date::String, date_field_name::String)
	dr_params = Paramlist()
	pub_date_dict = Dict{String, String}()
	pub_date_dict["gte"] = start_date
	pub_date_dict["lte"] = end_date
	range_dict = Dict{String, Any}()
	range_dict[date_field_name] = pub_date_dict
	push!(dr_params, Dict("range" => range_dict))
end
