module ElasticSearchClient
using Requests, JSON

import Requests: get, post, put, delete, options, json

typealias Paramlist Array{Dict{String, Any},1}
type Client
	connection_string::String
	function Client(host::String, port::Int64)
		new("http://$host:$port/")
	end
end

include("query_builder.jl")

function create_index(c::Client, index::String, document_field_name::String, content_field_name::String, date_field_name::String)
	resp = put(string(c.connection_string, index))
	#create the analyzer
	r = create_content_analyzer(c, index, "my_content_analyzer", 3)
	if haskey(r, "error")
		display(r["error"]["root_cause"])
	end

	#create a mapping to add an analyzer to document/content
	analyzer_english_dict = Dict{String, Any}("type"=>"text", "analyzer"=>"my_content_analyzer", "store"=>"true", "term_vector"=>"yes")
	english_field_dict = Dict{String, Any}("analyzed" => analyzer_english_dict)
	mapping_dict = Dict{String, Any}("type"=>"text", "fields"=>english_field_dict)
	content_dict = Dict{String, Any}(content_field_name => mapping_dict)
	pub_date_settings = Dict{String, Any}("type"=>"date", "store"=>"true")
	content_dict[date_field_name] = pub_date_settings
	prop_dict = Dict{String, Any}("properties"=>content_dict)

	r = put(string(c.connection_string, index,"/", "_mapping","/",document_field_name), data=JSON.json(prop_dict))
	r = json(r)
	if haskey(r, "error")
		display(r["error"]["root_cause"])
	end
	json(resp)
end

function create_content_analyzer(c::Client, index::String, analyzer_name::String, min_word_length::Int64)
	#close the index
	post(string(c.connection_string, index, "/_close"))
	#settings for min length filter
	min_length_filter_settings = Dict{String, Any}("type"=>"length", "min"=>min_word_length)
	min_length_filter = Dict{String, Any}("my_length_filter" => min_length_filter_settings)
	# my_stopwords_filter_settings = Dict{String, Any}("type"=>"stop", "stopwords_path"=>"/home/patrick/data/stopwords/stopwords_princeton.txt", "ignore_case"=>"true")
	# my_stopwords_filter = Dict{String, Any}("my_stopwords_filter" => my_stopwords_filter_settings)
	filter_settings = Dict{String, Any}("filter" => merge(min_length_filter))#, my_stopwords_filter))

	#create analyzer (use custom stopwords as soon as ACL is settled)
	filter_list = ["lowercase", "classic", "my_length_filter", "stop"]#"my_stopwords_filter"]
	analyzer_settings = Dict{String, Any}("type"=>"custom", "tokenizer"=>"classic","filter"=>filter_list)
	my_analyzer = Dict{String, Any}(analyzer_name=>analyzer_settings)
	analyzer = Dict{String, Any}("analyzer"=>my_analyzer)

	analysis = Dict{String, Any}("analysis"=>merge(filter_settings, analyzer))

	resp = put(string(c.connection_string, index, "/_settings"), data=JSON.json(analysis))

	#reopen the index
	post(string(c.connection_string, index, "/_open"))

	json(resp)
end

function delete_index(c::Client, index::String)
	resp = delete(string(c.connection_string, index))
	json(resp)
end

function create_index!(c::Client, index::String, document_field_name::String, content_field_name::String, date_field_name::String)
	delete_index(c, index)
	create_index(c, index, document_field_name, content_field_name, date_field_name)
end

function index_document(c::Client, index::String, document_field_name::String, document_data::Dict{String, Any})
	resp = post(string(c.connection_string, index, "/", document_field_name, "/"), data=JSON.json(document_data))
	json(resp)
end

function get_all_documents(c::Client, index::String, document_name::String)
	resp = get(string(c.connection_string, index, "/", document_name, "/_all"))
end

function get_document_by_id(c::Client, index::String, document_name::String, id::String)
	resp = get(string(c.connection_string, index, "/", document_name, "/", id))
	json(resp)
end

function get_document_field_by_id_and_name(c::Client, index::String, document_name::String, id::String, field_name::String)
	resp = get(string(c.connection_string, index, "/", document_name, "/", id,"?_source=",field_name))
	json(resp)["_source"][field_name]
end

function get_documents_by_id(c::Client, index::String, document_name::String, ids::Array{String,1}, stored_field_name::String)
	params = Dict{String, Any}("ids" => ids)
	resp = get(string(c.connection_string, index, "/", document_name, "/_mget?stored_fields=", stored_field_name), data=JSON.json(params))
	json(resp)["docs"]
end

function get_document_content_vector_by_id(c::Client, index::String, document_name::String, id::String, content_field::String)
	resp = get(string(c.connection_string, index, "/", document_name, "/", id,"/_termvectors"))
	json(resp)["term_vectors"][content_field]
end

function get_document_content_vectors_by_id(c::Client, index::String, document_name::String, ids::Array{String, 1})
	params = Dict{String, Any}("ids" => ids)
	resp = get(string(c.connection_string, index, "/", document_name,"/_mtermvectors"), data= JSON.json(params))
	json(resp)["docs"]
end
end #end module
